// AMD IOMMU initialization code.
//
// Copyright (C) 2010  Eduard - Gabriel Munteanu <eduard.munteanu@linux360.ro>
//
// This file may be distributed under the terms of the GNU LGPLv3 license.

#include "iommu.h"
#include "pci.h"
#include "types.h"

#define IOMMU_CAP_BAR_LOW       0x04
#define IOMMU_CAP_BAR_HIGH      0x08
#define IOMMU_CAP_RANGE         0x0C
#define IOMMU_CAP_MISC          0x10

static int iommu_bdf = -1;
static u8 iommu_cap_offset;
static u32 iommu_base;

void iommu_init(int bdf, u32 base)
{
    u8 ptr, cap, type;

    /* Only one IOMMU is supported. */
    if (iommu_bdf >= 0)
        return;

    foreachcap(bdf, ptr, cap) {
        type = pci_config_readb(bdf, cap);
        if (type == PCI_CAP_ID_SEC)
            break;
    }
    if (!cap)
        return;

    pci_config_writel(bdf, cap + IOMMU_CAP_RANGE, 0);
    pci_config_writel(bdf, cap + IOMMU_CAP_BAR_HIGH, 0);
    pci_config_writel(bdf, cap + IOMMU_CAP_BAR_LOW, base | 1);

    iommu_bdf = bdf;
    iommu_cap_offset = cap;
    iommu_base = base;
}

int iommu_get_bdf(void)
{
    return iommu_bdf;
}

u8 iommu_get_cap_offset(void)
{
    return iommu_cap_offset;
}

u32 iommu_get_misc(void)
{
    return pci_config_readw(iommu_bdf, iommu_cap_offset + IOMMU_CAP_MISC + 2);
}

u32 iommu_get_base(void)
{
    return iommu_base;
}

