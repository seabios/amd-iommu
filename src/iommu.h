#ifndef __IOMMU_H
#define __IOMMU_H

#include "types.h"

void iommu_init(int bdf, u32 base);
int iommu_get_bdf(void);
u8 iommu_get_cap_offset(void);
u32 iommu_get_misc(void);

#endif

